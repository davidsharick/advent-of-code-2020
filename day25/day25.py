#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """5764801
		17807724"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 14897079)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 0)
		pass

def parse_input(i):
	o = []
	for line in i:
		o.append(int(line.strip()))
	return o

def part1(i):
	card_key, door_key = i
	card_ls = 0
	card_subjvalue = 1
	while True:
		card_subjvalue *= 7
		card_subjvalue %= 20201227
		card_ls += 1
		if card_subjvalue == card_key:
			break
	door_ls = 0
	door_subjvalue = 1
	while True:
		door_subjvalue *= 7
		door_subjvalue %= 20201227
		door_ls += 1
		if door_subjvalue == door_key:
			break
	key_val = 1
	for i in range(card_ls):
		key_val *= door_key
		key_val %= 20201227
	return key_val

def part2(i):
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
