#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 5)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 8)
		pass

def parse_input(i):
	o = []
	for line in i:
		tokens = line.strip().split()
		c = (tokens[0], int(tokens[1]))
		o.append(c)
	return o

def part1(i):
	acc = 0
	pc = 0
	visited = Counter()
	while True:
		if visited[pc] != 0:
			return acc
		if pc == len(i):
			return acc
		visited[pc] += 1
		instr = i[pc]
		if instr[0] == "nop":
			pc += 1
			pass
		elif instr[0] == "acc":
			pc += 1
			acc += instr[1]
		elif instr[0] == "jmp":
			pc += instr[1]
		else:
			assert False
	return 0

def terminates(i):
	acc = 0
	pc = 0
	visited = Counter()
	while True:
		if pc == len(i) - 1:
			return True
		if visited[pc] != 0:
			return False
		visited[pc] += 1
		instr = i[pc]
		if instr[0] == "nop":
			pc += 1
			pass
		elif instr[0] == "acc":
			pc += 1
			acc += instr[1]
		elif instr[0] == "jmp":
			pc += instr[1]
		else:
			assert False
	return 0

def part2(i):
	for idx, inst in enumerate(i):
		if inst[0] == "nop":
			i2 = i.copy()
			i2[idx] = ("jmp", inst[1])
			if terminates(i2):
				return part1(i2)
		if inst[0] == "jmp":
			i2 = i.copy()
			i2[idx] = ("nop", inst[1])
			if terminates(i2):
				return part1(i2)
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
