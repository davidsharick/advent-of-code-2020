#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """BFFFBBFRRR
BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 820)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 120)
		pass

def parse_input(i):
	o = []
	for line in i:
		r = []
		for c in line.strip():
			r.append(c)
		o.append(r)
	return o

def part1(i):
	b = 0
	for bp in i:
		lo = 0
		hi = 127
		for c in bp[0:7]:
			if c == "F":
				hi = (lo + hi) // 2
			elif c == "B":
				lo = math.ceil((lo + hi) / 2)
			else:
				assert False, c
		assert lo == hi
		row = lo
		lo = 0
		hi = 7
		for c in bp[7:10]:
			if c == "L":
				hi = (lo + hi) // 2
			elif c == "R":
				lo = math.ceil((lo + hi) / 2)
			else:
				assert False, c
		assert lo == hi
		col = lo
		b = max(b, (8 * row) + col)
	return b

def part2(i):
	ids = []
	for bp in i:
		lo = 0
		hi = 127
		for c in bp[0:7]:
			if c == "F":
				hi = (lo + hi) // 2
			elif c == "B":
				lo = math.ceil((lo + hi) / 2)
			else:
				assert False, c
		assert lo == hi
		row = lo
		lo = 0
		hi = 7
		for c in bp[7:10]:
			if c == "L":
				hi = (lo + hi) // 2
			elif c == "R":
				lo = math.ceil((lo + hi) / 2)
			else:
				assert False, c
		assert lo == hi
		col = lo
		ids.append((8 * row) + col)
	ids = sorted(ids)
	c = ids[0]
	for curr in ids[1:]:
		if curr != c + 1:
			return c + 1
		c = curr

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
