#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 71)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 0)
		pass

def parse_input(i):
	num_blank = 0
	fields = {}
	tick = []
	other_tick = []
	for line in i:
		if len(line.strip()) == 0:
			num_blank += 1
		else:
			if num_blank == 0:
				colidx = line.find(":")
				oridx = line.find(" or ")
				field = line[:colidx]
				r1 = line[colidx + 2:oridx]
				r1 = tuple(int(x) for x in r1.split("-"))
				r2 = line[oridx + 4:]
				r2 = tuple(int(x) for x in r2.split("-"))
				fields[field] = (r1, r2)
			elif num_blank == 1:
				if "ticket" in line:
					pass
				else:
					tick = list(int(x) for x in line.strip().split(","))
			elif num_blank == 2:
				if "ticket" in line:
					pass
				else:
					other_tick.append(list(int(x) for x in line.strip().split(",")))
	return fields, tick, other_tick

def part1(i):
	fields, ticket, other_tickets = i
	o = 0
	for ti in other_tickets:
		o += find_invalid(fields, ti)
	return o

def find_invalid(fields, ticket):
	o = 0
	for val in ticket:
		valid = False
		for f in fields.values():
			for t in f:
				if val >= t[0] and val <= t[1]:
					valid = True
		if not valid:
			o += val
	return o

def part2(i):
	fields, ticket, other_tickets = i
	t2 = []
	for ti in other_tickets:
		if find_invalid(fields, ti) == 0 and 0 not in ti:
			t2.append(ti)
	invalids = {}
	for ti in t2:
		for f in fields.keys():
			seen_indices = []
			curr_idx = 0
			for token in ti:
				for t in fields[f]:
					if token >= t[0] and token <= t[1]:
						seen_indices.append(curr_idx)
				curr_idx += 1
			if len(seen_indices) != 20:
				exp = 0
				for i in seen_indices:
					if i != exp:
						missing = exp
						break
					exp += 1
				if missing is None:
					missing = exp
				try:
					invalids[f].append(missing)
				except:
					invalids[f] = [missing]
	li = []
	for k, v in invalids.items():
		v.sort()
		li.append((k, v))
	li.sort(key = lambda k: len(k[1]), reverse = True)
	known = {}
	used = []
	for item in li:
		for i in range(20):
			if i not in used and i not in item[1]:
				known[item[0]] = i
				used.append(i)
	o = 1
	for k, v in known.items():
		if k.startswith("departure"):
			o *= ticket[v]
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
