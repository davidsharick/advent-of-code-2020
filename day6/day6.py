#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """abc

a
b
c

ab
ac

a
a
a
a

b"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 11)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 6)
		pass

def parse_input(i):
	o = []
	c = []
	for line in i:
		if len(line.strip()) == 0:
			o.append(c)
			c = []
		else:
			c.append(line.strip())
	o.append(c)
	return o

def part1(i):
	o = 0
	for group in i:
		t = "".join(group)
		o += len(Counter(t).keys())
	return o

def part2(i):
	o = 0
	for group in i:
		c0 = Counter(group[0])
		if len(group) == 0:
			o += len(c0.keys())
		else:
			for p in group[1:]:
				c1 = Counter(p)
				for k in c0.keys():
					if c1[k] == 0:
						c0[k] = 0
			for k in c0.keys():
				if c0[k] != 0:
					o += 1
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
