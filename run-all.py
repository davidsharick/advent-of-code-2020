#!/usr/bin/env python3

import os
import subprocess
import sys
import timeit

t_begin = timeit.default_timer()
total_time = 0
all_times = []
for i in range(1, int(sys.argv[1]) + 1):
    t_start = timeit.default_timer()
    in_time = False
    times = []
    if len(sys.argv) > 2:
        time = 0
        a = str(subprocess.check_output([f"./day{i}/day{i}.py",  f"day{i}/day{i}-input.txt", "1"]))
        print(f"\n\n--------------------------DAY {i}--------------------------\n")
        print(f"ANSWERS:")
        lines = str(a[2:-1]).split("\\n")
        for ind, line in enumerate(lines): #ugly and maybe slow, but the best thing I've found for output to both the program and stdout
            if len(line.strip()) > 0:
                if "seconds" in lines[ind] and not in_time:
                    print("\nTIME:")
                    in_time = True
                if ind == 0: # brittle, won't work if a part 1 ans is multiline
                    print(f"Part 1 Answer:\n{line}")
                elif ind == 1:
                    print(f"Part 2 Answer:\n{line}")
                else:
                    print(line)
            if "seconds" in line:
                time += float(line.split()[2])
                times.append(float(line.split()[2]))
        all_times.append(times)
    else:
        os.system(f"./day{i}/day{i}.py day{i}/day{i}-input.txt")
    t_end = timeit.default_timer()
    if len(sys.argv) <= 2:
        print(f"Day {i} took {t_end - t_start} seconds to complete")
    else:
        print(f"Day {i} took {time} seconds to complete")
        total_time += time
    #print(f"Latency: {(t_end - t_start) - time}")
#print(f"Total time: {timeit.default_timer() - t_begin} seconds")
if len(sys.argv) > 2:
    print(f"\n\n----------------------------------------------------------\n")
    print(f"Total time: {total_time} seconds")
    print(f"Average: {total_time / (int(sys.argv[1]) * 2)} seconds per star, {total_time / (int(sys.argv[1]))} per day")
    print(f"\n")
    #print(all_times)