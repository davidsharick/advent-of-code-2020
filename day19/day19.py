#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		#self.assertEqual(part1(self.test), 2)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 12)
		pass

def parse_input(i):
	o = []
	rules = {}
	at_strs = False 
	for line in i:
		if at_strs:
			o.append(line.strip())
		else:
			if len(line.strip()) == 0:
				at_strs = True
				continue
			tokens = line.strip().split()
			rule_num = int(tokens[0][:-1])
			cur_rule = []
			full_rules = []
			for t in tokens[1:]:
				if t == "|":
					full_rules.append(tuple(cur_rule))
					cur_rule = []
				else:
					try:
						cur_rule.append(int(t))
					except:
						cur_rule.append(t)
			if len(cur_rule) != 0:
				full_rules.append(tuple(cur_rule))
			rules[rule_num] = tuple(full_rules)
	return o, rules

def part1(i):
	strs, rules = i
	x = all_permutations(rules, 0)
	o = 0
	for s in strs:
		if s in x:
			o += 1
	return o

def max_depth(rules, num):
	if type(rules[num][0][0]) is str:
		return 1
	else:
		b = 0
		for r in rules[num]:
			for r2 in r:
				b = max(b, max_depth(rules, r2))
		return b + 1

def all_permutations(rules, num):
	if type(rules[num][0][0]) is str:
		return [rules[num][0][0].strip("\"")]
	else:
		o = []
		for opt in rules[num]:
			str_sets = []
			for component in opt:
				str_sets.append(all_permutations(rules, component))
			for p in itertools.product(*str_sets):
				c = ""
				for s in p:
					c += s
				o.append(c)
		return o

def part2(i):
	strs, rules = i
	rules_31 = all_permutations(rules, 31)
	rules_42 = all_permutations(rules, 42)
	o = 0
	n = len(rules_31[0])
	for s in strs:
		tokens = list(s[i:i + n] for i in range(0, len(s), n)) # https://stackoverflow.com/questions/9475241/split-string-every-nth-character
		seen_swap = False
		valid = True
		tmap = []
		for t in tokens:
			if t in rules_31:
				tmap.append(31)
			elif t in rules_42:
				tmap.append(42)
			else:
				tmap.append(None)
		n_42 = 0
		n_31 = 0
		for t in tmap:
			if t == 31:
				if n_42 == 0:
					valid = False
					break
				n_31 += 1
			if t == 42:
				if n_31 != 0:
					valid = False
					break
				n_42 += 1
		if n_42 <= n_31 or n_42 == 0 or n_31 == 0:
			valid = False
		if valid:
			o += 1
	return o
			

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
