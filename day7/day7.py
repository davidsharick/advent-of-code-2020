#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 4)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 126)
		pass

def parse_input(i):
	bag_types = []
	bag_dict = {}
	for line in i:
		tokens = line.strip().split()
		bt = tokens[0] + " " + tokens[1]
		bag_types.append(bt)
		l = len(tokens)
		if l == 7:
			bag_dict[bt] = []
		else:
			assert l % 4 == 0
			n = (l - 4) // 4
			types = []
			for i in range(n):
				curr_t = tokens[5 + (4 * i)] + " " + tokens[6 + (4 * i)]
				curr_size = int(tokens[4 + (4 * i)])
				types.append((curr_t, curr_size))
			bag_dict[bt] = types
	return bag_types, bag_dict

def part1(i):
	o = 0
	for bt in i[0]:
		if bt == "shiny gold":
			continue
		if can_have_sg(bt, i[1]):
			o += 1
	return o

def can_have_sg(curr, dict):
	if curr == "shiny gold":
		return True
	elif len(dict[curr]) == 0:
		return False
	else:
		return any(can_have_sg(c[0], dict) for c in dict[curr])

def contained_bags(curr, dict):
	if len(dict[curr]) == 0:
		return 0
	else:
		o = 0
		for c in dict[curr]:
			o += c[1]
			o += (c[1] * contained_bags(c[0], dict))
		return o

def part2(i):
	return contained_bags("shiny gold", i[1])

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
