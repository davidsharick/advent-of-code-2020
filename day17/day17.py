#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """.#.
..#
###"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 112)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 848)
		pass

def parse_input(i):
	o = set()
	y = 0
	for line in i:
		x = 0
		for c in line.strip():
			if c == "#":
				o.add((y, x, 0))
			x += 1
		y += 1
	return o

def part1(i):
	lit = i
	for _ in range(6):
		new_lit = set()
		seen = Counter()
		for pos in lit:
			neighbors = grid.get_neighbors(3, pos, None, borderless = True)
			assert len(neighbors) == 26
			for n in neighbors:
				seen[n] += 1
		for k, v in seen.items():
			if k in lit and (v == 2 or v == 3):
				new_lit.add(k)
			elif k not in lit and v == 3:
				new_lit.add(k)
		lit = new_lit
	return len(lit)

def part2(i):
	lit = set()
	for p in i:
		lit.add((p[0], p[1], p[2], 0))
	for _ in range(6):
		new_lit = set()
		seen = Counter()
		for pos in lit:
			neighbors = grid.get_neighbors(4, pos, None, borderless = True)
			assert len(neighbors) == 80
			for n in neighbors:
				seen[n] += 1
		for k, v in seen.items():
			if k in lit and (v == 2 or v == 3):
				new_lit.add(k)
			elif k not in lit and v == 3:
				new_lit.add(k)
		lit = new_lit
	return len(lit)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
