#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###..."""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 20899048083289)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 273)
		pass

def parse_input(i):
	o = {}
	ct = 0
	curr = []
	for line in i:
		if "Tile" in line:
			o[ct] = tuple(curr)
			curr = []
			ct = int(line.strip().split()[1][:-1])
		elif len(line.strip()) > 0:
			curr.append(list(1 if c == "#" else 0 for c in line.strip()))
	o[ct] = tuple(curr)
	return o

def part1(i):
	bidc = Counter()
	tile_sets = {}
	for tid, tile in i.items():
		if len(tile) == 0:
			continue
		bids = all_edges(tile)
		for bid in bids:
			bidc[bid] += 1
		tile_sets[tid] = bids
	t = 1
	for k, v in tile_sets.items():
		c = 0
		for bid in v:
			if bidc[bid] == 1:
				c += 1
		if c == 4: #legitimately no idea why this works
			t *= k
	return t

def all_edges(tile, pri = False):
	l1 = lines.lines_on_grid((-1, 0), (10, 10), "down")
	l2 = lines.lines_on_grid((0, -1), (10, 10), "right")
	l3 = lines.lines_on_grid((-1, 9), (10, 10), "down")
	l4 = lines.lines_on_grid((9, -1), (10, 10), "right")
	l5 = list(reversed(l1))
	l6 = list(reversed(l2))
	l7 = list(reversed(l3))
	l8 = list(reversed(l4))
	ls = [l1, l2, l3, l4, l5, l6, l7, l8]
	#if pri:
	#	pr(ls)
	#	for i, l in enumerate(l3):
	#		y, x = l
	#		pr(tile[y][x])
	#		pr(tile[y][x] * int(math.pow(2, i)))
	ts = []
	for l in ls:
		t = 0
		for i, pos in enumerate(l):
			y, x = pos
			t += tile[y][x] * int(math.pow(2, i))
		ts.append(t)
	return ts

def part2(i):
	bidc = Counter()
	tile_sets = {}
	for tid, tile in i.items():
		if len(tile) == 0:
			continue
		bids = all_edges(tile)
		for bid in bids:
			bidc[bid] += 1
		tile_sets[tid] = bids
	t = 1
	cornersets = []
	edgesets = []
	full_thing = []
	all_tiles = tile_sets.copy()
	first = True
	while True:
		currcorn = []
		curredge = []
		newtilesets = {}
		newbidc = Counter()
		for k, v in tile_sets.items():
			c = 0
			for bid in v:
				if bidc[bid] == 1:
					c += 1
			if c >= 4:
				currcorn.append(k)
			elif c == 2:
				curredge.append(k)
			else:
				if len(tile_sets) == 1:
					break
				newtilesets[k] = v
				for bid in v:
					newbidc[bid] += 1
		edges, ories, corners, cornerories = get_ordering(currcorn, curredge, tile_sets, bidc)
		#if first:
		#	prettyprint_all(edges, corners, ories, cornerories, i)
		cornersets.append(currcorn)
		edgesets.append(curredge)
		tile_sets = newtilesets
		bidc = newbidc
		thing = (edges, ories, corners, cornerories)
		if first:
			full_thing.append((thing, 0))
		else:
			x = find_alignment(full_thing[-1][0], thing, i, all_tiles)
			full_thing.append((rotate_ring(thing, x), 0))
			#prettyprint_all(full_thing[-1][0][0], full_thing[-1][0][2], full_thing[-1][0][1], full_thing[-1][0][3], i)
		first = False
		if len(tile_sets.keys()) == 0:
			break
	full_tiles, full_ories = coalesce(full_thing)
	full_arr = []
	for y in range(len(full_tiles)):
		r = []
		for x in range(len(full_tiles[y])):
			r.append(apply_transform(i[full_tiles[y][x]], full_ories[y][x]))
		full_arr.append(r)
	#prettyprint_arr(full_arr)
	seamonster_image = remove_edges(full_arr)
	#prettyprint_basic(seamonster_image)
	for orie in range(8):
		new_image = apply_transform(seamonster_image, orie)
		x = num_seamonsters(new_image)
		if len(x) > 0:
			return not_in_monster(new_image, x)
	return 0

def get_ordering(corners, edges, tiles, bidc):
	topleft = corners[0]
	toprow = []
	assert len(edges) % 4 == 0
	size = len(edges) // 4
	if bidc[tiles[topleft][0]] == 2 and bidc[tiles[topleft][1]] == 2:
		rightedge = tiles[topleft][4] # 5
		bottomedge = tiles[topleft][5]
		tlorie = 2
	elif bidc[tiles[topleft][1]] == 2 and bidc[tiles[topleft][2]] == 2:
		rightedge = tiles[topleft][1] # 6
		bottomedge = tiles[topleft][6]
		tlorie = 1
	elif bidc[tiles[topleft][2]] == 2 and bidc[tiles[topleft][3]] == 2:
		rightedge = tiles[topleft][2] # 3
		bottomedge = tiles[topleft][3]
		tlorie = 0
	else:
		if len(corners) == 1:
			return [[], [], [], []], corners, [[], [], [], []], [0]
	ories = []
	orieset = []
	while len(toprow) < size:
		for tile in edges:
			if rightedge in tiles[tile] and tile not in toprow:
				t = tiles[tile]
				if t[0] == rightedge:
					rightedge = t[2]
					orie = 0
				elif t[1] == rightedge:
					rightedge = t[3]
					orie = 7
				elif t[2] == rightedge:
					rightedge = t[0]
					orie = 6
				elif t[3] == rightedge:
					rightedge = t[1]
					orie = 1
				elif t[4] == rightedge:
					rightedge = t[6]
					orie = 4
				elif t[5] == rightedge:
					rightedge = t[7]
					orie = 3
				elif t[6] == rightedge:
					rightedge = t[4]
					orie = 2
				elif t[7] == rightedge:
					rightedge = t[5]
					orie = 5
				else:
					assert False, "should never happen"
				toprow.append(tile)
				orieset.append(orie)
				break
	ories.append(orieset)
	orieset = []
	for t in corners[1:]:
		if rightedge in tiles[t]:
			topright = t
			t = tiles[t]
			if t[0] == rightedge:
				newbottom = t[3]
				trorie = 0#6
			elif t[1] == rightedge:
				newbottom = t[2]
				trorie = 7#1
			elif t[2] == rightedge:
				newbottom = t[7]
				trorie = 6#0
			elif t[3] == rightedge:
				newbottom = t[6]
				trorie = 1#7
			elif t[4] == rightedge:
				newbottom = t[1]
				trorie = 4#2
			elif t[5] == rightedge:
				newbottom = t[0]
				trorie = 5
			elif t[6] == rightedge:
				newbottom = t[5]
				trorie = 2#4
			elif t[7] == rightedge:
				newbottom = t[4]
				trorie = 3
	leftcol = []
	while len(leftcol) < size:
		for tile in edges:
			if bottomedge in tiles[tile] and tile not in leftcol:
				t = tiles[tile]
				if t[0] == bottomedge:
					bottomedge = t[2]
					orie = 7
				elif t[1] == bottomedge:
					bottomedge = t[3]
					orie = 0
				elif t[2] == bottomedge:
					bottomedge = t[0]
					orie = 3
				elif t[3] == bottomedge:
					bottomedge = t[1]
					orie = 4
				elif t[4] == bottomedge:
					bottomedge = t[6]
					orie = 1
				elif t[5] == bottomedge:
					bottomedge = t[7]
					orie = 6
				elif t[6] == bottomedge:
					bottomedge = t[4]
					orie = 5
				elif t[7] == bottomedge:
					bottomedge = t[5]
					orie = 2
				else:
					assert False, "should never happen"
				leftcol.append(tile)
				orieset.append(orie)
				break
	ories.append(orieset)
	orieset = []
	for t in leftcol:
		assert t not in toprow
	for t in toprow:
		assert t not in leftcol
	for t in corners[1:]:
		if bottomedge in tiles[t] and t != topright:
			bottomleft = t
			t = tiles[t]
			if t[0] == bottomedge:
				newright = t[3]
				blorie = 7
			elif t[1] == bottomedge:
				newright = t[2]
				blorie = 0
			elif t[2] == bottomedge:
				newright = t[7]
				blorie = 3
			elif t[3] == bottomedge:
				newright = t[6]
				blorie = 4
			elif t[4] == bottomedge:
				newright = t[1]
				blorie = 1
			elif t[5] == bottomedge:
				newright = t[0]
				blorie = 5
			elif t[6] == bottomedge:
				newright = t[5]
				blorie = 6
			elif t[7] == bottomedge:
				newright = t[4]
				blorie = 2
	for t in corners:
		if t != topleft and t != topright and t != bottomleft:
			bottomright = t
	rightedge = newright
	botrow = []
	while len(botrow) < size:
		for tile in edges:
			if rightedge in tiles[tile] and tile not in botrow:
				t = tiles[tile]
				if t[0] == rightedge:
					rightedge = t[2]
					orie = 0
				elif t[1] == rightedge:
					rightedge = t[3]
					orie = 7
				elif t[2] == rightedge:
					rightedge = t[0]
					orie = 6
				elif t[3] == rightedge:
					rightedge = t[1]
					orie = 1
				elif t[4] == rightedge:
					rightedge = t[6]
					orie = 4
				elif t[5] == rightedge:
					rightedge = t[7]
					orie = 3
				elif t[6] == rightedge:
					rightedge = t[4]
					orie = 2
				elif t[7] == rightedge:
					rightedge = t[5]
					orie = 5
				else:
					assert False, "should never happen"
				botrow.append(tile)
				orieset.append(orie)
				break
	ories.append(orieset)
	orieset = []
	rightcol = []
	bottomedge = newbottom
	while len(rightcol) < size:
		for tile in edges:
			if bottomedge in tiles[tile] and tile not in rightcol:
				t = tiles[tile]
				if t[0] == bottomedge:
					bottomedge = t[2]
					orie = 7
				elif t[1] == bottomedge:
					bottomedge = t[3]
					orie = 0
				elif t[2] == bottomedge:
					bottomedge = t[0]
					orie = 3
				elif t[3] == bottomedge:
					bottomedge = t[1]
					orie = 4
				elif t[4] == bottomedge:
					bottomedge = t[6]
					orie = 1
				elif t[5] == bottomedge:
					bottomedge = t[7]
					orie = 6
				elif t[6] == bottomedge:
					bottomedge = t[4]
					orie = 5
				elif t[7] == bottomedge:
					bottomedge = t[5]
					orie = 2
				else:
					assert False, "should never happen"
				rightcol.append(tile)
				orieset.append(orie)
				break
	ories.append(orieset)
	t = tiles[bottomright]
	if t[0] == rightedge:
		brorie = 0
	elif t[1] == rightedge:
		brorie = 7
	elif t[2] == rightedge:
		brorie = 6
	elif t[3] == rightedge:
		brorie = 1
	elif t[4] == rightedge:
		brorie = 4
	elif t[5] == rightedge:
		brorie = 3
	elif t[6] == rightedge:
		brorie = 2
	elif t[7] == rightedge:
		brorie = 5
	else:
		assert False, "should never happen"
	assert len(set(toprow + leftcol + botrow + rightcol)) == len(toprow + leftcol + botrow + rightcol) == size * 4 == sum(len(p) for p in ories)
	return ([toprow, leftcol, botrow, rightcol], ories, [topleft, topright, bottomleft, bottomright], [tlorie, trorie, blorie, brorie])

def apply_transform(tile, transform):
	new_tile = []
	for y in range(len(tile)):
		r = []
		for x in range(len(tile[0])):
			r.append(0)
		new_tile.append(r)
	reflect = transform >= 4
	rotation = transform % 4
	l = len(new_tile)
	for y in range(len(new_tile)):
		for x in range(len(new_tile[0])):
			if rotation == 0:
				new_tile[y][x] = tile[y][x]
			elif rotation == 1:
				new_tile[x][l - y - 1] = tile[y][x]
			elif rotation == 2:
				new_tile[l - y - 1][l - x - 1] = tile[y][x]
			elif rotation == 3:
				new_tile[l - x - 1][y] = tile[y][x]
	if reflect:
		return list(reversed(new_tile))
	else:
		return new_tile

def find_alignment(outer, inner, tilemap, bids):
	goal = outer[0][0][0]
	goal_orie = outer[1][0][0]
	bid_set = bids[goal]
	if goal_orie == 0:
		goal_bid = bid_set[3]
	elif goal_orie == 1:
		goal_bid = bid_set[6]
	elif goal_orie == 2:
		goal_bid = bid_set[5]
	elif goal_orie == 3:
		goal_bid = bid_set[0]
	elif goal_orie == 4:
		goal_bid = bid_set[1]
	elif goal_orie == 5:
		goal_bid = bid_set[4]
	elif goal_orie == 6:
		goal_bid = bid_set[7]
	elif goal_orie == 7:
		goal_bid = bid_set[2]
	else:
		assert False, "invalid goal orie"
	for corner in inner[2]:
		if goal_bid in bids[corner]:
			if corner == inner[2][0]:
				# top left, 0 or 7
				return 0
			elif corner == inner[2][1]:
				# top right, 3, or 6
				return 6
			elif corner == inner[2][2]:
				# bottom left, 1 or 4
				return 4
			elif corner == inner[2][3]:
				# bottom right, 2 or 5
				return 5 if len(inner[1][0]) < 4 else 2 # lol
	return 0

def rotate_ring(ring, orie):
	if orie == 0:
		return ring
	edges, ories, corners, cornerories = ring
	newories = []
	newcornerories = []
	for orieset in ories:
		newset = []
		for orie1 in orieset:
			rot1 = orie1 % 4
			ref1 = orie1 // 4
			rot2 = orie % 4
			ref2 = orie // 4
			if ref1:
				newrot = (rot1 - rot2) % 4
				newref = 0 if ref2 else ref1
			else:
				newrot = (rot1 + rot2) % 4
				newref = ref2
			neworie = (4 * newref) + newrot
			newset.append(neworie)
		newories.append(newset)
	ories = newories
	for orie1 in cornerories:
		rot1 = orie1 % 4
		ref1 = orie1 // 4
		rot2 = orie % 4
		ref2 = orie // 4
		if ref1:
			newrot = (rot1 - rot2) % 4
			newref = 0 if ref2 else ref1
		else:
			newrot = (rot1 + rot2) % 4
			newref = ref2
		neworie = (4 * newref) + newrot
		newcornerories.append(neworie)
	cornerories = newcornerories
	for i in range(orie % 4):
		# rotate all
		# edges: 0 -> 3, 1 -> 0 rev, 2 -> 1, 3 -> 2 rev
		# corners: 0 -> 1, 1 -> 3, 3 -> 2, 2 -> 0
		newedges = [0,0,0,0]
		newedges[3] = edges[0]
		newedges[0] = list(reversed(edges[1]))
		newedges[1] = edges[2]
		newedges[2] = list(reversed(edges[3]))
		newories = [0,0,0,0]
		newories[3] = ories[0]
		newories[0] = list(reversed(ories[1]))
		newories[1] = ories[2]
		newories[2] = list(reversed(ories[3]))
		newcorners = [0,0,0,0]
		newcorners[1] = corners[0]
		newcorners[3] = corners[1]
		newcorners[2] = corners[3]
		newcorners[0] = corners[2]
		newcornerories = [0,0,0,0]
		newcornerories[1] = cornerories[0]
		newcornerories[3] = cornerories[1]
		newcornerories[2] = cornerories[3]
		newcornerories[0] = cornerories[2]
		edges = newedges
		ories = newories
		corners = newcorners
		cornerories = newcornerories
	if orie >= 4:
		newedges = [0,0,0,0]
		newedges[2] = edges[0]
		newedges[1] = list(reversed(edges[1]))
		newedges[0] = edges[2]
		newedges[3] = list(reversed(edges[3]))
		newories = [0,0,0,0]
		newories[2] = ories[0]
		newories[1] = list(reversed(ories[1]))
		newories[0] = ories[2]
		newories[3] = list(reversed(ories[3]))
		newcorners = [0,0,0,0]
		newcorners[0] = corners[2]
		newcorners[3] = corners[1]
		newcorners[2] = corners[0]
		newcorners[1] = corners[3]
		newcornerories = [0,0,0,0]
		newcornerories[0] = cornerories[2]
		newcornerories[3] = cornerories[1]
		newcornerories[2] = cornerories[0]
		newcornerories[1] = cornerories[3]
		edges = newedges
		ories = newories
		corners = newcorners
		cornerories = newcornerories
	return (edges, ories, corners, cornerories)

def prettyprint_row(tiles, ories, tilemap, gap = 0):
	tileset = []
	for i, t in enumerate(tiles):
		tileset.append(apply_transform(tilemap[t], ories[i]))
	for y in range(len(tileset[0])):
		curr_row = ""
		for i, tile in enumerate(tileset):
			for x in range(len(tileset[0][0])):
				if tile[y][x] == 1:
					curr_row += "#"
				else:
					curr_row += " "
			curr_row += "|"
			if i < len(tileset) - 1:
				curr_row += ((" " * len(tileset[0][0])) + "|") * gap
		print(curr_row)

def prettyprint_all(edges, corners, ories, cornerories, tilemap):
	prettyprint_row([corners[0]] + edges[0] + [corners[1]], [cornerories[0]] + ories[0] + [cornerories[1]], tilemap)
	print("-" * (11 * (len(edges[0]) + 2)))
	for i in range(len(edges[1])):
		prettyprint_row([edges[1][i]] + [edges[3][i]], [ories[1][i]] + [ories[3][i]], tilemap, gap = len(edges[0]))
		print("-" * (11 * (len(edges[0]) + 2)))
	prettyprint_row([corners[2]] + edges[2] + [corners[3]], [cornerories[2]] + ories[2] + [cornerories[3]], tilemap)

def prettyprint_full(full_map, full_ories, tilemap):
	for i in range(len(full_map)):
		if i > 0:
			print("-" * (11 * len(full_map)))
		prettyprint_row(full_map[i], full_ories[i], tilemap)

def coalesce(full_thing):
	total_ories = []
	total_tiles = []
	size = len(full_thing[0][0][0][0]) + 2
	for y in range(size):
		r = []
		r2 = []
		for x in range(size):
			r.append(0)
			r2.append(0)
		total_tiles.append(r)
		total_ories.append(r2)
	curr_max = size - 1
	curr_min = 0
	curr_size = size - 2
	for part in full_thing:
		edges, ories, corners, cornerories = part[0]
		total_tiles[curr_min][curr_min] = corners[0]
		total_tiles[curr_min][curr_max] = corners[1]
		total_tiles[curr_max][curr_min] = corners[2]
		total_tiles[curr_max][curr_max] = corners[3]
		total_ories[curr_min][curr_min] = cornerories[0]
		total_ories[curr_min][curr_max] = cornerories[1]
		total_ories[curr_max][curr_min] = cornerories[2]
		total_ories[curr_max][curr_max] = cornerories[3]
		for i in range(curr_size):
			total_tiles[curr_min][curr_min + i + 1] = edges[0][i]
			total_ories[curr_min][curr_min + i + 1] = ories[0][i]
		for i in range(curr_size):
			total_tiles[curr_min + i + 1][curr_min] = edges[1][i]
			total_ories[curr_min + i + 1][curr_min] = ories[1][i]
		for i in range(curr_size):
			total_tiles[curr_max][curr_min + i + 1] = edges[2][i]
			total_ories[curr_max][curr_min + i + 1] = ories[2][i]
		for i in range(curr_size):
			total_tiles[curr_min + i + 1][curr_max] = edges[3][i]
			total_ories[curr_min + i + 1][curr_max] = ories[3][i]
		curr_max -= 1
		curr_min += 1
		curr_size -= 2
	return total_tiles, total_ories

def remove_edges(arr):
	out = []
	size = 8
	for y in range(len(arr) * size):
		r = []
		for x in range(len(arr[0]) * size):
			r.append(-1)
		out.append(r)
	for y in range(len(arr)):
		for x in range(len(arr[0])):
			sq = arr[y][x]
			for y2 in range(1, size + 1):
				for x2 in range(1, size + 1):
					out[(size * y) + y2 - 1][(size * x) + x2 - 1] = sq[y2][x2]
	return out

def validate(arr):
	pass
	# lol

def prettyprint_arr(arr):
	for y in range(len(arr)):
		for y2 in range(len(arr[y][0])):
			s = ""
			for x in range(len(arr[y])):
				for x2 in range(len(arr[y][x][0])):
					if arr[y][x][y2][x2] == 1:
						s += "#"
					else:
						s += " "
				s += "|"
			print(s)
		print("-")

def prettyprint_basic(arr):
	for y in range(len(arr)):
		s = ""
		for x in range(len(arr[0])):
			s += "#" if arr[y][x] else " "
		print(s)

def num_seamonsters(image):
	size = len(image)
	opts = []
	for y in range(1, size - 1):
		for x in range(18, size - 1):
			if image[y][x] == 1 and image[y - 1][x] == 1 and image[y][x - 1] == 1 and image[y][x + 1] == 1:
				opts.append((y, x))
	return seamonsters_from_opts(image, opts)

def seamonsters_from_opts(image, opts):
	diffs = [(1, -2), (1, -5), (0, -6), (0, -7), (1, -8), (1, -11), (0, -12), (0, -13), (1, -14), (1, -17), (0, -18)]
	out = []
	for opt in opts:
		valid = True
		for diff in diffs:
			new_y = opt[0] + diff[0]
			new_x = opt[1] + diff[1]
			if image[new_y][new_x] == 0:
				valid = False
				break
		if valid:
			out.append(opt)
	return out

def not_in_monster(image, monsters):
	diffs = [(0, 0), (-1, 0), (0, -1), (0, 1), (1, -2), (1, -5), (0, -6), (0, -7), (1, -8), (1, -11), (0, -12), (0, -13), (1, -14), (1, -17), (0, -18)]
	monster_parts = set()
	for monster in monsters:
		for diff in diffs:
			new_pos = (monster[0] + diff[0], monster[1] + diff[1])
			monster_parts.add(new_pos)
	out = 0
	for y in range(len(image)):
		for x in range(len(image)):
			if image[y][x] == 1 and (y, x) not in monster_parts:
				out += 1
	return out


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
