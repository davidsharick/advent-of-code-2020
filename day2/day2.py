#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 2)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 1)
		pass

def parse_input(i):
	o = []
	for line in i:
		n = []
		tokens = line.strip().split()
		r = tokens[0].split("-")
		n.append((int(r[0]), int(r[1])))
		n.append(tokens[1][0])
		n.append(tokens[2])
		o.append(n)
	return o

def part1(i):
	valid = 0
	for pw in i:
		c = Counter(pw[2])
		amt = c[pw[1]]
		if amt >= pw[0][0] and amt <= pw[0][1]:
			valid += 1
	return valid

def part2(i):
	valid = 0
	for pw in i:
		v0 = pw[2][pw[0][0] - 1]
		v1 = pw[2][pw[0][1] - 1]
		if v0 == pw[1] and v1 != pw[1] or v1 == pw[1] and v0 != pw[1]:
			valid += 1
	return valid

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
