#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1 + 2 * 3 + 4 * 5 + 6"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 71)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 231)
		pass

class Tree:
	def __init__(self, op, left, right):
		self.op = op
		self.left = left
		self.right = right
	
	def __init__(self, line):
		tokens = line.split()
	
	def collapse():
		if self.left is None and self.right is None:
			return self.op
		else:
			if self.op == "+":
				return self.left.collapse() + self.right.collapse()
			elif self.op == "*":
				return self.left.collapse() * self.right.collapse()

def parse_input(i):
	o = []
	for line in i:
		o.append(line.strip())
	return o


def part1(i):
	o = 0
	for line in i:
		o += eval(ast.unparse(Reverter().visit(ast.parse(line.replace("*", "-")))))
	return o

class Reverter(ast.NodeTransformer):
	def visit_BinOp(self, node):
		if type(node.op) is ast.Sub:
			return ast.BinOp(
				left = self.visit(node.left),
				op = ast.Mult(),
				right = self.visit(node.right),
			)
		else:
			return ast.BinOp(
				left = self.visit(node.left),
				op = node.op,
				right = self.visit(node.right),
			)

class SuperReverter(ast.NodeTransformer):
	def visit_BinOp(self, node):
		if type(node.op) is ast.Sub:
			return ast.BinOp(
				left = self.visit(node.left),
				op = ast.Mult(),
				right = self.visit(node.right),
			)
		elif type(node.op) is ast.Div:
			return ast.BinOp(
				left = self.visit(node.left),
				op = ast.Add(),
				right = self.visit(node.right),
			)
		else:
			return ast.BinOp(
				left = self.visit(node.left),
				op = node.op,
				right = self.visit(node.right),
			)

def part2(i):
	o = 0
	for line in i:
		o += eval(ast.unparse(SuperReverter().visit(ast.parse(line.replace("*", "-").replace("+", "/")))))
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
