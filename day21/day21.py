#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 5)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), "mxmxvkd,sqjhc,fvjkl")
		pass

def parse_input(i):
	o = []
	for line in i:
		tokens = line.split()
		ingredients = []
		allergens = []
		in_allergens = False
		for t in tokens:
			if "contains" in t:
				in_allergens = True
			elif in_allergens:
				allergens.append(t[:-1])
			else:
				ingredients.append(t)
		o.append((ingredients, allergens))
	return o

def part1(i):
	allergen_counts = Counter()
	allergen_occurrences = defaultdict(list)
	allergen_ingredients = defaultdict(Counter)
	ingrs = Counter()
	for idx, recipe in enumerate(i):
		for allergen in recipe[1]:
			allergen_counts[allergen] += 1
			allergen_occurrences[allergen].append(idx)
			for ingr in recipe[0]:
				allergen_ingredients[allergen][ingr] += 1
		for ingr in recipe[0]:
			ingrs[ingr] += 1
	safe = []
	for ingr in ingrs.keys():
		valid = True
		for allergen in allergen_counts.keys():
			if allergen_ingredients[allergen][ingr] >= allergen_counts[allergen]:
				valid = False
		if valid:
			safe.append(ingr)
	o = sum(ingrs[ingr] for ingr in safe)
	return o

def part2(i):
	allergen_counts = Counter()
	allergen_occurrences = defaultdict(list)
	allergen_ingredients = defaultdict(Counter)
	ingrs = Counter()
	for idx, recipe in enumerate(i):
		for allergen in recipe[1]:
			allergen_counts[allergen] += 1
			allergen_occurrences[allergen].append(idx)
			for ingr in recipe[0]:
				allergen_ingredients[allergen][ingr] += 1
		for ingr in recipe[0]:
			ingrs[ingr] += 1
	safe = []
	for ingr in ingrs.keys():
		valid = True
		for allergen in allergen_counts.keys():
			if allergen_ingredients[allergen][ingr] >= allergen_counts[allergen]:
				valid = False
		if valid:
			safe.append(ingr)
	allergen_map = defaultdict(list)
	known = {}
	for ingr in ingrs.keys():
		if ingr not in safe:
			for allergen, count in allergen_counts.items():
				if allergen_ingredients[allergen][ingr] == count:
					allergen_map[allergen].append(ingr)
	while len(known.keys()) < len(allergen_counts.keys()):
		for allergen, possibilities in allergen_map.items():
			if len(possibilities) == 1:
				known[allergen] = possibilities[0]
		for allergen, ingr in known.items():
			for allergen, possibilities in allergen_map.items():
				if ingr in possibilities:
					possibilities.remove(ingr)
	l = list(known.keys())
	o = ""
	for allergen in sorted(l):
		o += known[allergen] + ","
	return o[:-1]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
