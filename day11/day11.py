#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 37)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 26)
		pass

def parse_input(i):
	o = []
	for line in i:
		r = []
		for c in line.strip():
			if c == ".":
				r.append(0)
			elif c == "L":
				r.append(1)
			else:
				assert False
		o.append(r)
	return o

def part1(i):
	o = 0
	while True:
		i, nc = run_change(i)
		if nc == 0:
			for y in range(len(i)):
				for x in range(len(i[y])):
					if i[y][x] == 2:
						o += 1
			return o
	return o

def run_change(cgrid):
	ng = []
	max_y = len(cgrid)
	max_x = len(cgrid[0])
	num_changed = 0
	for _ in range(max_y):
		r = []
		for _ in range(max_x):
			r.append(0)
		ng.append(r)
	for y in range(max_y):
		for x in range(max_x):
			curr = cgrid[y][x]
			if curr == 0:
				ng[y][x] = 0
				continue
			ns = grid.grid_neighbors((y, x), (0, max_y - 1), (0, max_x - 1))
			num_occ = sum(cgrid[n[0]][n[1]] == 2 for n in ns)
			if curr == 1:
				if num_occ == 0:
					ng[y][x] = 2
					num_changed += 1
				else:
					ng[y][x] = 1
			elif curr == 2:
				if num_occ >= 4:
					ng[y][x] = 1
					num_changed += 1
				else:
					ng[y][x] = 2
	return ng, num_changed

def part2(i):
	o = 0
	idx = 0
	while True:
		i, nc = run_change_2(i)
		if nc == 0:
			for y in range(len(i)):
				for x in range(len(i[y])):
					if i[y][x] == 2:
						o += 1
			return o
	return o

def run_change_2(cgrid):
	ng = []
	max_y = len(cgrid)
	max_x = len(cgrid[0])
	dims = (max_y, max_x)
	num_changed = 0
	directions = ["upleft", "up", "upright", "left", "right", "downleft", "down", "downright"]
	for _ in range(max_y):
		r = []
		for _ in range(max_x):
			r.append(0)
		ng.append(r)
	for y in range(max_y):
		for x in range(max_x):
			curr = cgrid[y][x]
			if curr == 0:
				ng[y][x] = 0
				continue
			num_occ = 0
			for d in directions:
				l = lines.lines_on_grid((y, x), dims, d)
				for y2, x2 in l:
					if cgrid[y2][x2] == 2:
						num_occ += 1
						break
					elif cgrid[y2][x2] == 1:
						break
			if curr == 1:
				if num_occ == 0:
					ng[y][x] = 2
					num_changed += 1
				else:
					ng[y][x] = 1
			elif curr == 2:
				if num_occ >= 5:
					ng[y][x] = 1
					num_changed += 1
				else:
					ng[y][x] = 2
	return ng, num_changed

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
