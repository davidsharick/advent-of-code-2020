#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Graph:
	def __init__(self, nodes, adjacencies):
		self.nodes = nodes
		self.adj = adjacencies

def dijkstra(graph, start):
	nodes = graph.nodes
	adj = graph.adj
	dist = {start: 0}
	prev = {}
	queue = [start]
	for n in nodes:
		if n != start:
			dist[n] = math.inf
			prev[n] = None
			queue.append(n)
	
	while len(queue) > 0:
		queue = sorted(queue, key = lambda v: dist[v])
		smallest = queue[0]
		queue = queue[1:]
		for n in adj[smallest]:
			new_dist = dist[smallest] + n[1] # 0: dest, 1: weight 
			if new_dist < dist[n[0]]:
				dist[n[0]] = new_dist
				prev[n[0]] = smallest
	return dist, prev