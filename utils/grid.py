#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

@lru_cache(None)
def grid_neighbors(position, y_range, x_range, wrap = False, borderless = False, corners = True):
	return get_neighbors(2, position, (y_range, x_range), corners = corners, borderless = borderless, wrap = wrap)

@lru_cache(None)
def get_neighbors(dimension, position, ranges, corners = True, borderless = False, wrap = False):
	if len(position) != dimension:
		raise ValueError(f"Length of position ({len(position)}) should equal dimension {dimension}")
	if not borderless:
		if len(ranges) != dimension:
			raise ValueError(f"Length of ranges ({len(ranges)}) should equal dimension {dimension}")
		for r in ranges:
			if len(r) != 2:
				raise ValueError(f"Length of range {r} should be 2")
	
	options = []
	for i in range(dimension):
		options.append((-1, 0, 1))
	new_positions = []
	max_sum = dimension if corners else dimension - 1
	for diff in itertools.product(*options):
		s = sum(abs(p) for p in diff)
		if s != 0 and s <= max_sum:
			new_positions.append(tuple(position[p] + diff[p] for p in range(dimension)))
	
	out = []
	for pos in new_positions:
		if borderless:
			out.append(pos)
			continue
		valid = True
		for dim, p in enumerate(pos):
			if p < ranges[dim][0] or p > ranges[dim][1]:
				valid = False
				break
		if valid:
			out.append(pos)
	return out

#print(get_neighbors(3, (0, 0, 0), ((0, 5), (0, 5), (0, 5)), corners = True, borderless = False))


#print(grid_neighbors((1, 1), (0, 2), (0, 1)))

def grid_dijkstra(grid, start, path_function, corners = True, wrap = False, borderless = False):
	nodes = []
	adj = {}
	for y in range(len(grid)):
		for x in range(len(grid[y])):
			pos = (y, x)
			nodes.append(pos)
			neighbors = grid_neighbors(grid, (0, len(grid) - 1), (0, len(grid[y ] - 1)), wrap = wrap, borderless = borderless, corners = corners)
			for ni in neighbors:
				weight = path_function(pos, ni)
				if weight >= 0:
					try:
						adj[n].append([ni, weight])
					except:
						adj[n] = [[ni, weight]]
	dist = {start: 0}
	prev = {}
	queue = [start]
	for n in nodes:
		if n != start:
			dist[v] = math.inf
			prev[v] = None
			queue.append(n)
	
	while len(queue) > 0:
		queue = sorted(queue, key = lambda v: dist[v])
		smallest = queue[0]
		queue = queue[1:]
		for n in adj[smallest]:
			new_dist = dist[smallest] + n[1]
			if new_dist < dist[n[0]]:
				dist[n[0]] = new_dist
				prev[n[0]] = smallest
	return dist, prev

def expand_grid(grid, sides):
	# sides are tuple of (dir, amt)
	return grid
