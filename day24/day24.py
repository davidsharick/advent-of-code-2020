#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 10)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 2208)
		pass

def parse_input(i):
	o = []
	for line in i:
		instrs = parse_line(line.strip())
		o.append(instrs)
	return o

def parse_line(line):
	curr_instr = ""
	instrs = []
	curr_idx = 0
	while curr_idx < len(line):
		curr_char = line[curr_idx]
		if curr_char in ['e', 'w']:
			instrs.append(curr_char)
			curr_idx += 1
		else:
			instrs.append(line[curr_idx:curr_idx + 2])
			curr_idx += 2
	return instrs

def part1(i):
	changes = {"e": (0, 1), "w": (0, -1), "se": (1, 1), "sw": (1, 0), "ne": (-1, 0), "nw": (-1, -1)}
	flipped = Counter()
	for route in i:
		pos = (0, 0)
		for instr in route:
			pos = (pos[0] + changes[instr][0], pos[1] + changes[instr][1])
		flipped[pos] += 1
	o = 0
	for p in flipped.keys():
		if flipped[p] % 2 == 1:
			o += 1
	return o

def hex_neighbors(pos):
	changes = {"e": (0, 1), "w": (0, -1), "se": (1, 1), "sw": (1, 0), "ne": (-1, 0), "nw": (-1, -1)}
	o = []
	for _, v in changes.items():
		o.append((pos[0] + v[0], pos[1] + v[1]))
	return o

def part2(i):
	changes = {"e": (0, 1), "w": (0, -1), "se": (1, 1), "sw": (1, 0), "ne": (-1, 0), "nw": (-1, -1)}
	flipped = Counter()
	alwaysflip = []
	for route in i:
		pos = (0, 0)
		for instr in route:
			pos = (pos[0] + changes[instr][0], pos[1] + changes[instr][1])
			alwaysflip.append(pos)
		flipped[pos] += 1
	p = 0
	for k, v in flipped.items():
		if v % 2 == 1:
			p += 1
	o = 0
	for i in range(100):
		neighbors = Counter()
		for tile, flips in flipped.items():
			if flips % 2 != 1:
				continue
			ns = hex_neighbors(tile)
			for n in ns:
				neighbors[n] += 1
		for n, amt in neighbors.items():
			if (amt == 0 or amt > 2) and flipped[n] % 2 == 1:
				flipped[n] += 1
			elif amt == 2 and flipped[n] % 2 == 0:
				flipped[n] += 1
		for t in flipped.keys():
			if neighbors[t] == 0 and flipped[t] % 2 == 1:
				flipped[t] += 1
		p = 0
		for k, v in flipped.items():
			if v % 2 == 1:
				p += 1
	for k, v in flipped.items():
		if v % 2 == 1:
			o += 1
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
