#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """939
7,13,x,x,59,x,31,19"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 295)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 1068781)
		pass

def parse_input(i):
	o = []
	p = 0
	for idx, line in enumerate(i):
		if idx == 0:
			p = int(line.strip())
		else:
			o = list(int(x) if x != "x" else x for x in line.strip().split(","))
	return (p, o)

def part1(i):
	start_t, buses = i
	best = math.inf
	o = 0
	for b in buses:
		if b == "x":
			continue
		time_in_loop = start_t % b
		earliest = start_t + (b - time_in_loop)
		if earliest < best:
			best = earliest
			o = (b - time_in_loop) * b
	return o

def part2(i):
	buses = i[1]
	offsets = Counter()
	idx = 0
	for b in buses:
		if b != "x":
			offsets[b] = b - idx
			if offsets[b] == b:
				offsets[b] = 0
		idx += 1
	buses = sorted([b for b in buses if b != "x"], reverse = True)
	c1 = buses[0]
	c2 = buses[1]
	i0 = 0
	sol = 0
	while i0 <= len(buses) - 2: # https://en.wikipedia.org/wiki/Chinese_remainder_theorem#Using_the_existence_construction
		c2 = buses[i0 + 1]
		a, b = get_coeffs(c1, c2)
		sol = (b * c1 * offsets[c2]) + (a * c2 * offsets[c1])
		sol %= (c1 * c2)
		c1 = (c1 * c2)
		offsets[c1] = sol
		c2 = sol
		i0 += 1
	return sol

def get_coeffs(a, b): # https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
	old_r = a
	r = b
	old_s = 1
	s = 0
	old_t = 0
	t = 1

	while r != 0:
		quot = old_r // r
		old_r, r = r, old_r - (quot * r)
		old_s, s = s, old_s - (quot * s)
		old_t, t = t, old_t - (quot * t)
	return (old_t, old_s)


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
