#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"""
		self.test = parse_input(self.testinput.split("\n"))

		self.testinput2 = """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 165)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test2), 208)
		pass

def parse_input(i):
	o = []
	for line in i:
		tokens = line.strip().split()
		if line[:2] == "ma":
			o.append(tokens[2])
		elif line[:2] == "me":
			o.append((tokens[0], tokens[2]))
	return o

def part1(i):
	ormask = 0
	andmask = int(math.pow(2, 36)) - 1
	mem = Counter()
	for instr in i:
		if type(instr) is str:
			ormask_str = instr.replace("X", "0")
			andmask_str = instr.replace("X", "1")
			ormask = int(ormask_str, 2)
			andmask = int(andmask_str, 2)
		else:
			addr = instr[0]
			idx0 = addr.find("[")
			idx1 = addr.find("]")
			addr = int(addr[idx0 + 1:idx1])
			val = int(instr[1])
			mem[addr] = (val & andmask) | ormask
	o = 0
	for v in mem.values():
		o += v
	return o

def part2(i):
	memmask = 0
	mem = Counter()
	bits = []
	for instr in i:
		if type(instr) is str:
			bits = []
			ormask_str = instr.replace("X", "0")
			memmask = int(ormask_str, 2)
			for idx in range(35, -1, -1):
				if instr[35 - idx] == "X":
					bits.append(idx)
		else:
			addr = instr[0]
			idx0 = addr.find("[")
			idx1 = addr.find("]")
			addr = int(addr[idx0 + 1:idx1])
			val = int(instr[1])
			addr |= memmask
			for b in bits:
				if int(math.pow(2, b)) & addr != 0:
					addr -= int(math.pow(2, b))
			opts = all_addrs(addr, bits)
			for ad in opts:
				mem[ad] = val
	o = 0
	for v in mem.values():
		o += v
	return o

def all_addrs(base_addr, bits):
	if len(bits) == 0:
		return [base_addr]
	else:
		b = bits[0]
		o1 = all_addrs(base_addr, bits[1:])
		o2 = all_addrs(base_addr + int(math.pow(2, b)), bits[1:])
		return o1 + o2

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
