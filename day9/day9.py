#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test, 5), 127)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test, 5), 62)
		pass

def parse_input(i):
	o = []
	for line in i:
		o.append(int(line.strip()))
	return o

def part1(i, pre_length):
	prev = []
	for idx in range(pre_length):
		prev.append(i[idx])
	idx = pre_length
	while True:
		valid = twosum(prev, i[idx])
		if not valid:
			return i[idx]
		prev.pop(0)
		prev.append(i[idx])
		idx += 1
	return 0

def twosum(lst, target):
	l2 = Counter(lst)
	for l in lst:
		need = target - l
		if l2[need] != 0 and need != l:
			return True
	return False

def part2(i, pre_length):
	goal = part1(i, pre_length)
	curr_start = 0
	while True:
		# try to 
		curr_end = curr_start + 1
		running_total = i[curr_start] + i[curr_end]
		while True:
			if running_total == goal:
				return p2_val(i, curr_start, curr_end)
			elif running_total > goal:
				break
			else:
				curr_end += 1
				running_total += i[curr_end]
		curr_start += 1
	return 0

def p2_val(i, start, end):
	mi = math.inf
	ma = 0
	for idx in range(start, end + 1):
		mi = min(mi, i[idx])
		ma = max(ma, i[idx])
	return mi + ma

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i, 25))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i, 25))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
