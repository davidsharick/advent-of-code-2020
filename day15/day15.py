#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """0,3,6"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 436)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 175594)
		pass

def parse_input(i):
	for line in i:
		o = list(int(x) for x in line.strip().split(","))
	return o

def part1(i):
	t = 1
	last = Counter()
	seen_twice = set()
	ln = i[0]
	while True:
		if t <= len(i):
			cn = i[t - 1]
			ln = cn
			last[cn] = t
		else:
			if last[ln] == 0:
				cn = 0
				if last[cn] != 0 and cn not in seen_twice:
					seen_twice.add(cn)
				else:
					last[ln] = t - 1
			else:
				cn = (t - 1) - last[ln]
				last[ln] = t - 1
				if last[cn] != 0:
					seen_twice.add(cn)
			ln = cn
		if t == 2020:
			return cn
		t += 1
	return 0

def part2(i):
	t = 1
	last = Counter()
	seen_twice = set()
	ln = i[0]
	while True:
		if t <= len(i):
			cn = i[t - 1]
			ln = cn
			last[cn] = t
		else:
			if last[ln] == 0:
				cn = 0
				if last[cn] != 0 and cn not in seen_twice:
					seen_twice.add(cn)
				else:
					last[ln] = t - 1
			else:
				cn = (t - 1) - last[ln]
				last[ln] = t - 1
				if last[cn] != 0:
					seen_twice.add(cn)
			ln = cn
		if t == 30000000:
			return cn
		t += 1
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
