#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """16
10
15
5
1
11
7
19
6
12
4"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 7 * 5)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 8)
		pass

def parse_input(i):
	o = []
	for line in i:
		o.append(int(line.strip()))
	return o

def part1(i):
	l = sorted(i)
	assert len(l) == len(set(l))
	dev_jolt = l[-1] + 3
	diffs_1 = 0
	diffs_3 = 1
	if l[0] == 1:
		diffs_1 += 1
	elif l[0] == 3:
		diffs_3 += 1
	curr_joltage = l[0]
	for idx in range(1, len(l)):
		diff = l[idx] - curr_joltage
		if diff == 3:
			diffs_3 += 1
		elif diff == 1:
			diffs_1 += 1
		curr_joltage += diff
	return diffs_3 * diffs_1

def part2(i):
	l = sorted(i)
	l.insert(0, 0)
	l.append(l[-1] + 3)
	return num_combos(tuple(l), len(l) - 1)

@lru_cache(None)
def num_combos(l, max_idx):
	next_idx = max_idx - 1
	o = 0
	if max_idx <= 1:
		return 1
	if l[max_idx] - l[next_idx] < 3:
		o += num_combos(l, next_idx)
		next_idx -= 1
		if l[max_idx] - l[next_idx] <= 3:
			o += num_combos(l, next_idx)
			next_idx -= 1
			if l[max_idx] - l[next_idx] == 3:
				o += num_combos(l, next_idx)
	else:
		o += num_combos(l, next_idx)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
