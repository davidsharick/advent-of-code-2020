#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """F10
N3
F7
R90
F11"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 25)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 286)
		pass

def parse_input(i):
	o = []
	for line in i:
		action = line[0]
		num = int(line.strip()[1:])
		o.append((action, num))
	return o

def part1(i):
	facing = 0
	pos = [0, 0]
	for instr in i:
		action = instr[0]
		num = instr[1]
		if action == "N":
			pos[0] += num
		elif action == "E":
			pos[1] += num
		elif action == "W":
			pos[1] -= num
		elif action == "S":
			pos[0] -= num
		elif action == "L":
			facing = (facing + num) % 360
		elif action == "R":
			facing = (facing - num) % 360
		elif action == "F":
			frad = math.radians(facing)
			y = int(math.sin(frad))
			x = int(math.cos(frad))
			pos[0] += (y * num)
			pos[1] += (x * num)
	return abs(pos[0]) + abs(pos[1])

def part2(i):
	facing = 0
	pos = [0, 0]
	waypos = [1, 10]
	for instr in i:
		action = instr[0]
		num = instr[1]
		if action == "N":
			waypos[0] += num
		elif action == "E":
			waypos[1] += num
		elif action == "W":
			waypos[1] -= num
		elif action == "S":
			waypos[0] -= num
		elif action == "L":
			way_y = waypos[0]
			way_x = waypos[1]
			if num == 90:
				waypos = [way_x, -way_y]
			elif num == 180:
				waypos = [-way_y, -way_x]
			elif num == 270:
				waypos = [-way_x, way_y]
			else:
				assert False, instr
		elif action == "R":
			way_y = waypos[0]
			way_x = waypos[1]
			if num == 90:
				waypos = [-way_x, way_y]
			elif num == 180:
				waypos = [-way_y, -way_x]
			elif num == 270:
				waypos = [way_x, -way_y]
			else:
				assert False, instr
		elif action == "F":
			pos[0] += (waypos[0] * num)
			pos[1] += (waypos[1] * num)
	return abs(pos[0]) + abs(pos[1])

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
