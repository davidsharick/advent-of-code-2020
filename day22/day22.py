#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 306)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 291)
		pass

def parse_input(i):
	p1 = []
	p2 = []
	in_p2 = False
	for line in i:
		if "Player 2" in line:
			in_p2 = True
		elif "Player 1" in line:
			pass
		elif len(line.strip()) == 0:
			pass
		else:
			if in_p2:
				p2.append(int(line.strip()))
			else:
				p1.append(int(line.strip()))
	return p1, p2

def part1(i):
	p1, p2 = i
	while len(p1) > 0 and len(p2) > 0:
		p1_card = p1.pop(0)
		p2_card = p2.pop(0)
		if p1_card > p2_card:
			p1.append(p1_card)
			p1.append(p2_card)
		else:
			p2.append(p2_card)
			p2.append(p1_card)
	score = 0
	winner = p1 if len(p1) > 0 else p2
	for idx, val in enumerate(reversed(winner)):
		score += (val * (idx + 1))
	return score

def part2(i):
	decks = run_recursive_game(tuple(i[0]), tuple(i[1]), True, 1)
	winner_deck = decks[0] if len(decks[0]) > 0 else decks[1]
	score = 0
	for idx, val in enumerate(reversed(winner_deck)):
		score += (val * (idx + 1))
	return score

@lru_cache(None)
def run_recursive_game(p1, p2, outermost = False, depth = 0):
	seen = set()
	p1 = list(p1)
	p2 = list(p2)
	while True:
		t = (tuple(p1), tuple(p2))
		if t in seen:
			if not outermost:
				return "p1"
			else:
				break
		if len(p1) == 0:
			if not outermost:
				return "p2"
			else:
				break
		if len(p2) == 0:
			if not outermost:
				return "p1"
			else:
				break
		p1_card = p1.pop(0)
		p2_card = p2.pop(0)
		seen.add(t)
		if len(p1) >= p1_card and len(p2) >= p2_card:
			winner = run_recursive_game(tuple(p1[:p1_card]), tuple(p2[:p2_card]), depth = depth + 1)
			if winner == "p1":
				p1.append(p1_card)
				p1.append(p2_card)
			else:
				p2.append(p2_card)
				p2.append(p1_card)
		else:
			if p1_card > p2_card:
				p1.append(p1_card)
				p1.append(p2_card)
			else:
				p2.append(p2_card)
				p2.append(p1_card)
	return p1, p2

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
