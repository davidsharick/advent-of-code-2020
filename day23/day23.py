#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import ast
import collections
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph
import utils.lines as lines
pr = print

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """389125467"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "67384529")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 149245887792)
		pass

def parse_input(i):
	o = []
	for line in i:
		for c in line.strip():
			o.append(int(c))
	return o

def part1(i):
	cups = collections.deque(i)
	curr_idx = 0
	for _ in range(100):
		curr_cup = cups[curr_idx]
		next_1 = cups[(curr_idx + 1) % 9]
		next_2 = cups[(curr_idx + 2) % 9]
		next_3 = cups[(curr_idx + 3) % 9]
		nexts = [next_1, next_2, next_3]
		dest_cup = curr_cup
		while dest_cup == curr_cup or dest_cup in nexts:
			dest_cup = (dest_cup - 1)
			if dest_cup == 0:
				dest_cup = 9
		for n in nexts:
			cups.remove(n)
		next_cup = cups[(cups.index(curr_cup) + 1) % 6]
		spot = cups.index(dest_cup)
		for n in reversed(nexts):
			cups.insert(spot + 1, n)
		if spot > curr_idx:
			spot += 3
		curr_idx = cups.index(next_cup)
	start_idx = cups.index(1)
	s = ""
	for idx in range(start_idx + 1, start_idx + 9):
		s += str(cups[idx % 9])
	return s

def part2(i):
	cups1 = i
	curr_idx = 0
	for new_val in range(10, 1000001):
		cups1.append(new_val)
	assert len(cups1) == 1000000
	cups = [] # index = value, value = index of next in list
	for _ in range(1000000):
		cups.append(-1)
	for idx, cup in enumerate(cups1):
		value = cup
		next_idx = (idx + 1) % len(cups)
		cups[value - 1] = cups1[next_idx]
	assert -1 not in cups
	assert len(cups) == 1000000
	curr_cup = i[0]
	for i in range(10000000):
		next1 = cups[curr_cup - 1]
		next2 = cups[next1 - 1]
		next3 = cups[next2 - 1]
		nexts = [next1, next2, next3]
		cups[curr_cup - 1] = cups[next3 - 1]
		dest_cup = curr_cup - 1
		if dest_cup == 0:
			dest_cup = 1000000
		while dest_cup == curr_cup or dest_cup in nexts:
			dest_cup -= 1
			if dest_cup == 0:
				dest_cup = 1000000
		cups[next3 - 1] = cups[dest_cup - 1]
		cups[dest_cup - 1] = next1
		curr_cup = cups[curr_cup - 1]
	next_cup = cups[0]
	cup_after = cups[next_cup - 1]
	return next_cup * cup_after

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
