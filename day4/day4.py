#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 2)
		pass
		
	def test_part2(self):
		#self.assertEqual(part2(self.test), y)
		pass

def parse_input(i):
	passports = []
	d = {}
	for line in i:
		if len(line.strip()) == 0:
			passports.append(d)
			d = {}
		else:
			for t in line.split():
				k, v = t.split(":")
				d[k] = v
	passports.append(d)
	return passports

def part1(i):
	want = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
	o = 0
	for p in i:
		v = True
		for w in want:
			if w not in p.keys():
				v = False
				break
		if v:
			o += 1
	return o

def validate_passport(p):
	# assume it has the 7 needed fields
	byr = p["byr"]
	try:
		byr = int(byr)
		if byr < 1920 or byr > 2020:
			return False
	except:
		return False
	iyr = p["iyr"]
	try:
		iyr = int(iyr)
		if iyr < 2010 or iyr > 2020:
			return False
	except:
		return False
	eyr = p["eyr"]
	try:
		eyr = int(eyr)
		if eyr < 2020 or eyr > 2030:
			return False
	except:
		return False
	hgt = p["hgt"]
	if hgt.endswith("cm"):
		try:
			hgt = int(hgt[:-2])
			if hgt < 150 or hgt > 193:
				return False
		except:
			return False
	elif hgt.endswith("in"):
		try:
			hgt = int(hgt[:-2])
			if hgt < 59 or hgt > 76:
				return False
		except:
			return False
	else:
		return False
	hcl = p["hcl"]
	if len(hcl) != 7:
		return False
	if hcl[0] != "#":
		return False
	for c in hcl[1:]:
		if c not in "0123456789abcdef":
			return False
	if p["ecl"] not in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
		return False
	pid = p["pid"]
	if len(pid) != 9:
		return False
	for c in pid:
		if c not in "0123456789":
			return False
	return True

def part2(i):
	want = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
	o = 0
	for p in i:
		v = True
		for w in want:
			if w not in p.keys():
				v = False
				break
		if v:
			if validate_passport(p):
				o += 1
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
