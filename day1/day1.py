#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1721
979
366
299
675
1456"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 514579)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 241861950)
		pass

def parse_input(i):
	x = []
	for line in i:
		sl = line.strip()
		if len(sl) > 0:
			x.append(int(line.strip()))
	return x

def part1(i):
	x = Counter(i)
	for k in x.keys():
		need = 2020 - k
		if x[need] != 0:
			return k * need
	return 0

def twosum(counter, target, exclude):
	for k in counter.keys():
		if k == exclude:
			continue
		need = target - k
		if need == exclude:
			continue
		if counter[need] != 0:
			return k, need
	return 0, 0

def part2(i):
	x = Counter(i)
	for k in x.keys():
		need = 2020 - k
		b1, b2 = twosum(x, need, k)
		if b1 != 0 and b2 != 0:
			return b1 * b2 * k
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
