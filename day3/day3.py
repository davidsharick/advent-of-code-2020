#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid
import utils.graph as graph

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 7)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 336)
		pass

def parse_input(i):
	g = []
	for line in i:
		r = []
		for c in line.strip():
			if c == "#":
				r.append(1)
			elif c == ".":
				r.append(0)
			else:
				assert False, c
		g.append(r)
	return g

def part1(i):
	y = 0
	x = 0
	t = 0
	while True:
		x += 3
		y += 1
		if y >= len(i):
			break
		if i[y][x % len(i[y])] == 1:
			t += 1
	return t

def try_slope(i, down, right):
	y = 0
	x = 0
	t = 0
	while True:
		x += right
		y += down
		if y >= len(i):
			break
		if i[y][x % len(i[y])] == 1:
			t += 1
	return t

def part2(i):
	slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
	t = 1
	for s in slopes:
		t *= try_slope(i, s[0], s[1])
	return t

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
